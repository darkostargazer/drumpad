# README #

Drumpad turns your mac trackpad into a drumpad by dividing your trackpad into areas and connecting the sounds of your choice to the different areas.

### How do I get set up? ###

Clone the repo and run 'pip install -r requirements.txt' in the root.

Put the sound files you want to use as the drumpad sounds in a folder named grid_sounds in the root of the project. The trackpad divide itself into as many areas as are needed to match the number of sounds. The sound files nust be of a format supported by the pygame media library, .ogg seems to work very well, .wav not so much.

Start the drumpad by running:
python drumpad.py
