import os
from collections import namedtuple

import pygame
from pip._vendor.distlib.compat import raw_input

from trackpad import Trackpad


class ItemGrid(object):
    """
    Represents a grid divided into areas, where each area is connected to an item.
    """
    Point = namedtuple('Point', ['x', 'y'])
    Dim = namedtuple('Dim', ['w', 'h'])

    def __init__(self, items, width=100, height=100):
        self.height = height
        self.width = width

        self.dim = self._calculate_dim(len(items))
        self.items = {}
        for index, item in enumerate(items):
            box = self._get_box(index)
            self.items[box] = pygame.mixer.Sound(item)

    @classmethod
    def _calculate_dim(cls, length):
        if length < 3:
            return ItemGrid.Dim(length, 1)
        if length % 2 == 0:
            return ItemGrid.Dim(length / 2, 2)
        return ItemGrid.Dim((length + 1) / 2, 2)

    @property
    def box_dim(self):
        return ItemGrid.Dim(self.width / self.dim.w, self.height / self.dim.h)

    def _get_box_indexes(self, index):
        x = index % self.dim.w
        y = int(index / self.dim.w)
        return ItemGrid.Dim(x, y)

    def _get_box(self, index):
        indexes = self._get_box_indexes(index)
        dim = self.box_dim
        return self._Box(ItemGrid.Point(indexes.w * dim.w, indexes.h * dim.h), ItemGrid.Dim(dim.w, dim.h))

    def get_item(self, x, y):
        """
        Get the item corresponding to the area that contains the specified coords.
        """
        for box, sound in self.items.items():
            if box.is_within(x, y):
                return sound
        return self.items.values()[0]

    class _Box(object):
        def __init__(self, position, dimension):
            self.pos = position
            self.dim = dimension

        def is_within(self, x, y):
            return self.pos.x < x < self.pos.x + self.dim.w and self.pos.y < y < self.pos.y + self.dim.h

        def __str__(self):
            return '<Box at {0} with size: {1}>'.format(self.pos, self.dim)


class SoundGrid(ItemGrid):
    def __init__(self, path='grid_sounds'):
        sound_files = [os.path.join(path, file_) for file_ in os.listdir(path)][:16]
        super(SoundGrid, self).__init__(sound_files)


class Differ(object):
    def __init__(self, threshold=5):
        self.positions = None
        self.threshold = threshold

    def _is_within_threshold(self, val1, val2):
        return val1 - self.threshold < val2 < val1 + self.threshold

    def _is_missing(self, pos):
        match = False
        for position in self.positions:
            if self._is_within_threshold(position[0], pos[0]) and \
                    self._is_within_threshold(position[1], pos[1]):
                match = True
        return not match

    def has_changed(self, positions):
        """
        Check if the positions has changed within the allowed threshold.
        :param positions:
        :return: True if changed, False otherwise.
        """
        if self.positions is None:
            self.positions = []
            return False

        for pos in positions:
            if self._is_missing(pos):
                self.positions = positions
                return True
        self.positions = positions
        return False

    @staticmethod
    def get_first(positions):
        x = positions[0][0] if len(positions) > 0 else 0
        y = positions[0][1] if len(positions) > 0 else 0
        return ItemGrid.Point(x, y)


if __name__ == '__main__':

    # TODO: Adapt these to source sounds
    pygame.mixer.init(44100, -16, 2, 2048)

    grid = SoundGrid()
    differ = Differ()

    @Trackpad.MTContactCallbackFunction
    def on_track(device, data_ptr, n_fingers, timestamp, frame):
        positions = []
        grouped_positions = [[] for _ in range(n_fingers)]

        for i in range(n_fingers):
            data = data_ptr[i]
            positions.append((data.normalized.position.x * 100, data.normalized.position.y * 100))
            grouped_positions[i].append((data.normalized.position.x * 100, data.normalized.position.y * 100))

        if differ.has_changed(positions):
            for pos in grouped_positions:
                first = differ.get_first(pos)
                grid.get_item(first.x, first.y).play()

        return 0

    Trackpad(on_track)

    try:
        raw_input('Press enter to exit.')
    except KeyboardInterrupt:
        pass

    pygame.mixer.quit()
